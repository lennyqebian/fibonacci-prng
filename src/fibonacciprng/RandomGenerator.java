package fibonacciprng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author leonardoquevedo
 */
public class RandomGenerator {

    protected double N_0;

    protected double N_1;

    protected double m;

    protected String operator;

    protected String noDecimals = "%.0f";
    
    protected final ScriptEngineManager manager;

    protected final ScriptEngine engine;

    RandomGenerator(double N_0, double N_1, double m, String operation) {
        this.N_0 = N_0;
        this.N_1 = N_1;
        this.m = m;
        this.operator = operation;
        this.manager = new ScriptEngineManager();
        this.engine = this.manager.getEngineByName("JavaScript");
    }

    public double[] random(int n) throws Exception {
        if (!(n > 0)) {
            throw new Exception("¡La cantidad de números aleatorios a generar debe ser mayor que cero (0)!");
        }
        ArrayList<Double> N = new ArrayList();
        double[] U = new double[n];
        int s = 2;
        int r = 1;
        int next;
        N.add(this.N_0);
        N.add(this.N_1);

        for (int i = 0; i < n; i++) {
            next = s + i;
            N.add(this.runJsOperation(N.get(next - s), N.get(next - r)));
            U[i] = N.get(next) / this.m;
        }
        return U;
    }

    public String randomStr(int n, int decimalPlaces) throws Exception {
        double[] U = this.random(n);
        String format;

        if (!(decimalPlaces > 1)) {
            throw new Exception("¡El número de dígitos decimales debería ser por lo menos 2!");
        }
        format = "%." + decimalPlaces + "f";

        if (U.length > 1) {
            List<String> output = Arrays.stream(U).boxed().map((aDouble) -> {
                return String.format(format, aDouble);
            }).collect(Collectors.toList());
            return "Números aleatorios generados: \n\n"
                    + String.join("\n", output);
        }
        return "Número aleatorio generado: " + String.format(format, U[0]);
    }

    public String randomStr(int n) throws Exception {
        return randomStr(n, 4);
    }

    public double random() throws Exception {
        return this.random(1)[0];
    }
    
    protected double runJsOperation(double stateS, double stateR) throws ScriptException, ClassCastException {
        String script = "(" + stateS + " " + this.operator + " " + stateR + ") % " + this.m + ";";
        return (double) this.engine.eval(script);
    }
}
