package fibonacciprng;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;

/**
 *
 * @author leonardoquevedo
 */
public class Main extends javax.swing.JFrame {

    /**
     * Creates new form MainGUI
     */
    public Main() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlInput = new javax.swing.JPanel();
        lblN = new javax.swing.JLabel();
        spnN = new javax.swing.JSpinner();
        lblSeed0 = new javax.swing.JLabel();
        spnSeed0 = new javax.swing.JSpinner();
        jSeparator1 = new javax.swing.JSeparator();
        btnGenerate = new javax.swing.JButton();
        btnClearInput = new javax.swing.JButton();
        btnSeed0 = new javax.swing.JButton();
        lblDecimalPlaces = new javax.swing.JLabel();
        spnDecimalPlaces = new javax.swing.JSpinner();
        lblSeed1 = new javax.swing.JLabel();
        spnSeed1 = new javax.swing.JSpinner();
        btnSeed1 = new javax.swing.JButton();
        lblModulus = new javax.swing.JLabel();
        spnModulus = new javax.swing.JSpinner();
        btnModulus = new javax.swing.JButton();
        lblOperation = new javax.swing.JLabel();
        cbxOperation = new javax.swing.JComboBox<>();
        pnlOutput = new javax.swing.JPanel();
        sclOutput = new javax.swing.JScrollPane();
        txaOutput = new javax.swing.JTextArea();
        btnClearOutput = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Fibonacci: Generador de números pseudo-aleatorios");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        pnlInput.setBorder(javax.swing.BorderFactory.createTitledBorder("Entradas"));

        lblN.setText("Números aleatorios a generar: ");

        spnN.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        spnN.setNextFocusableComponent(spnDecimalPlaces);

        lblSeed0.setText("Semilla N₀: ");

        spnSeed0.setModel(new javax.swing.SpinnerNumberModel(this.getAutoSeed(3), 0L, null, 1));
        spnSeed0.setEditor(new javax.swing.JSpinner.NumberEditor(spnSeed0, "#"));
        spnSeed0.setNextFocusableComponent(btnSeed0);

        btnGenerate.setText("Generar");
        btnGenerate.setNextFocusableComponent(btnClearInput);
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        btnClearInput.setText("Limpiar");
        btnClearInput.setNextFocusableComponent(txaOutput);
        btnClearInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearInputActionPerformed(evt);
            }
        });

        btnSeed0.setText("Auto.");
        btnSeed0.setToolTipText("Hora del sistema (UNIX timestamp)");
        btnSeed0.setNextFocusableComponent(btnGenerate);
        btnSeed0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeed0ActionPerformed(evt);
            }
        });

        lblDecimalPlaces.setText("Decimales: ");

        spnDecimalPlaces.setModel(new javax.swing.SpinnerNumberModel(10, 2, null, 1));
        spnDecimalPlaces.setNextFocusableComponent(spnSeed0);

        lblSeed1.setText("Semilla N₁: ");

        spnSeed1.setModel(new javax.swing.SpinnerNumberModel(this.getAutoSeed(4), 0L, null, 1));
        spnSeed1.setEditor(new javax.swing.JSpinner.NumberEditor(spnSeed1, "#"));
        spnSeed1.setNextFocusableComponent(btnSeed0);

        btnSeed1.setText("Auto.");
        btnSeed1.setToolTipText("Hora del sistema (UNIX timestamp)");
        btnSeed1.setNextFocusableComponent(btnGenerate);
        btnSeed1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeed1ActionPerformed(evt);
            }
        });

        lblModulus.setText("Módulo m: ");

        spnModulus.setModel(new javax.swing.SpinnerNumberModel(this.getAutoSeed(5), 0L, null, 1));
        spnModulus.setEditor(new javax.swing.JSpinner.NumberEditor(spnModulus, "#"));
        spnModulus.setNextFocusableComponent(btnSeed0);

        btnModulus.setText("Auto.");
        btnModulus.setToolTipText("Hora del sistema (UNIX timestamp)");
        btnModulus.setNextFocusableComponent(btnGenerate);
        btnModulus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModulusActionPerformed(evt);
            }
        });

        lblOperation.setText("Operación o: ");

        cbxOperation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Suma (+)", "Resta (-)", "Multiplicación (*)" }));

        javax.swing.GroupLayout pnlInputLayout = new javax.swing.GroupLayout(pnlInput);
        pnlInput.setLayout(pnlInputLayout);
        pnlInputLayout.setHorizontalGroup(
            pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(pnlInputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInputLayout.createSequentialGroup()
                        .addComponent(btnClearInput, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(btnGenerate, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE))
                    .addGroup(pnlInputLayout.createSequentialGroup()
                        .addComponent(lblN)
                        .addGap(18, 18, 18)
                        .addComponent(spnN, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addComponent(lblDecimalPlaces)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(spnDecimalPlaces, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlInputLayout.createSequentialGroup()
                        .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblSeed1)
                            .addComponent(lblSeed0))
                        .addGap(18, 18, 18)
                        .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(pnlInputLayout.createSequentialGroup()
                                .addComponent(spnSeed0, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSeed0))
                            .addGroup(pnlInputLayout.createSequentialGroup()
                                .addComponent(spnSeed1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnSeed1)))
                        .addGap(24, 24, 24)
                        .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblModulus)
                            .addComponent(lblOperation))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(spnModulus)
                            .addComponent(cbxOperation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnModulus)))
                .addContainerGap())
        );
        pnlInputLayout.setVerticalGroup(
            pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblN)
                    .addComponent(spnN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDecimalPlaces)
                    .addComponent(spnDecimalPlaces, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblModulus)
                        .addComponent(spnModulus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnModulus))
                    .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblSeed0)
                        .addComponent(spnSeed0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSeed0)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSeed1)
                    .addComponent(spnSeed1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSeed1)
                    .addComponent(lblOperation)
                    .addComponent(cbxOperation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClearInput)
                    .addComponent(btnGenerate))
                .addContainerGap())
        );

        pnlOutput.setBorder(javax.swing.BorderFactory.createTitledBorder("Salida"));

        txaOutput.setEditable(false);
        txaOutput.setColumns(20);
        txaOutput.setRows(5);
        txaOutput.setNextFocusableComponent(btnClearOutput);
        sclOutput.setViewportView(txaOutput);

        btnClearOutput.setText("Limpiar");
        btnClearOutput.setNextFocusableComponent(btnExit);
        btnClearOutput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearOutputActionPerformed(evt);
            }
        });

        btnExit.setText("Salir");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlOutputLayout = new javax.swing.GroupLayout(pnlOutput);
        pnlOutput.setLayout(pnlOutputLayout);
        pnlOutputLayout.setHorizontalGroup(
            pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOutputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addGroup(pnlOutputLayout.createSequentialGroup()
                        .addComponent(btnClearOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(sclOutput))
                .addContainerGap())
        );
        pnlOutputLayout.setVerticalGroup(
            pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOutputLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sclOutput, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClearOutput)
                    .addComponent(btnExit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(17, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(pnlInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClearOutputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearOutputActionPerformed
        this.txaOutput.setText(null);
    }//GEN-LAST:event_btnClearOutputActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        this.dispose();
        System.exit(0);
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnClearInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearInputActionPerformed
        JSpinner.NumberEditor spnNEditor;
        spnNEditor = (JSpinner.NumberEditor) this.spnN.getEditor();
        this.spnN.setValue(spnNEditor.getModel().getMinimum());
        this.btnSeed0.doClick();
        this.btnSeed1.doClick();
        this.btnModulus.doClick();
        spnNEditor.getTextField().grabFocus();
    }//GEN-LAST:event_btnClearInputActionPerformed

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        int n = Integer.parseInt(this.spnN.getValue().toString());
        double N_0 = Double.parseDouble(this.spnSeed0.getValue().toString());
        double N_1 = Double.parseDouble(this.spnSeed1.getValue().toString());
        double m = Double.parseDouble(this.spnModulus.getValue().toString());
        String operation = this.cbxOperation.getSelectedItem().toString();
        Matcher matcher = Pattern.compile("\\((.*?)\\)").matcher(operation);
        
        while (matcher.find()) {
            operation = matcher.group(1);
        }
        int decimalPlaces = Integer.parseInt(this.spnDecimalPlaces.getValue().toString());

        try {
            RandomGenerator generator = new RandomGenerator(N_0, N_1, m, operation);
            String U = generator.randomStr(n, decimalPlaces);
            this.txaOutput.setText(U);
        } catch (Exception ex) {
            StringWriter stringWriter = new StringWriter();
            ex.printStackTrace(new PrintWriter(stringWriter));
            this.txaOutput.setText(stringWriter.toString());
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            this.showErrorDialog(ex.getMessage(), "¡Oops!");
        }
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void btnSeed0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeed0ActionPerformed
        this.spnSeed0.setValue(this.getAutoSeed(3));
    }//GEN-LAST:event_btnSeed0ActionPerformed

    private void btnSeed1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeed1ActionPerformed
        this.spnSeed1.setValue(this.getAutoSeed(4));
    }//GEN-LAST:event_btnSeed1ActionPerformed

    private void btnModulusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModulusActionPerformed
        this.spnModulus.setValue(this.getAutoSeed(5));
    }//GEN-LAST:event_btnModulusActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearInput;
    private javax.swing.JButton btnClearOutput;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnModulus;
    private javax.swing.JButton btnSeed0;
    private javax.swing.JButton btnSeed1;
    private javax.swing.JComboBox<String> cbxOperation;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblDecimalPlaces;
    private javax.swing.JLabel lblModulus;
    private javax.swing.JLabel lblN;
    private javax.swing.JLabel lblOperation;
    private javax.swing.JLabel lblSeed0;
    private javax.swing.JLabel lblSeed1;
    private javax.swing.JPanel pnlInput;
    private javax.swing.JPanel pnlOutput;
    private javax.swing.JScrollPane sclOutput;
    private javax.swing.JSpinner spnDecimalPlaces;
    private javax.swing.JSpinner spnModulus;
    private javax.swing.JSpinner spnN;
    private javax.swing.JSpinner spnSeed0;
    private javax.swing.JSpinner spnSeed1;
    private javax.swing.JTextArea txaOutput;
    // End of variables declaration//GEN-END:variables

    protected void showErrorDialog(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }
    
    protected void showErrorDialog(String message) {
        this.showErrorDialog(message, "¡ERROR!");
    }
    
    protected long getAutoSeed(int factor) {
        return ((System.currentTimeMillis() / 1000L * 256) % (System.currentTimeMillis() / (1000L * 35 * factor))) + 1;
    }
}
